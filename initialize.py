from api.models import db
from api.models.todo import Todo


def create_table():
    """Create database"""
    db.create_tables([Todo])

if __name__ == '__main__':
    create_table()
