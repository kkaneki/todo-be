from pyramid.view import view_config, view_defaults

from api.decorators.validation import exclude_and_validate
from api.helpers.db import is_int
from api.helpers.response import make_response
from api.models.todo import Todo
from api.schemas.todo import TodoSchema


@view_defaults(route_name='todos')
class ToDolViews(object):
    def __init__(self, request):
        self.json_data = request.json if request.json else {}
        self.response = request.response
        self.request = request
        self.view_name = 'todos_views'

    @exclude_and_validate(TodoSchema)
    @view_config(request_method=('POST', 'OPTIONS'),
                 route_name='create_ticket',
                 renderer='json')
    def create_ticket(self):
        """Create todo ticket"""
        ticket = Todo(**self.json_data)
        ticket.save()

        self.response.status_code = 201
        return make_response(ticket.info())

    @view_config(request_method='PATCH',
                 route_name='update_ticket',
                 renderer='json')
    def update_ticket(self):
        """Mark ticket as completed"""
        ticket_id = self.request.matchdict['id']

        if not is_int(ticket_id):
            self.response.status_code = 400
            return {}

        if not Todo.ticket_exists(ticket_id):
            self.response.status_code = 404
            return {}

        ticket = Todo.get(id=ticket_id)
        ticket.completed = True
        ticket.save()

        self.response.status_code = 200
        return {}
