from api.helpers.response import make_response


def exclude_keys(data, fields):
    """Makes an new dictionary
    only with allowed fields

        Args:
            data An request dictionary
            fields Of an schema allowed fields

        Returns:
            An new dictionary only with allowed fields
    """
    new_data = {}
    for k, v in data.items():
        if k not in fields:
            continue
        new_data[k] = v
    return new_data


def exclude_and_validate(schema):
    """An decorator for a request validation

      Note: If a validation failed, we set the 400 HTTP error
      into our `view.response` object, and returns an errors.

      Args:
          schema A schema which need to validate
    """
    def check(func):
        def wrapper(*args, **kwargs):
            view = args[0]
            data = exclude_keys(view.json_data, schema.__fields__)
            validated_data, errors = schema().load(data)
            if errors:
                view.response.status_code = 400
                return make_response(errors, 'error')
            view.json_data = validated_data
            return func(*args, **kwargs)
        return wrapper
    return check
