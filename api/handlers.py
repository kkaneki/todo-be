"""Error helpers"""

from api.helpers.response import make_response


def integreti_error_view(context, request):
    """Handle peewee's IntegrityError"""
    request.response.status_code = 400
    return make_response({'name': ['Body of the ticket already exists']}, 'error')
