def make_response(data, key: str='data'):
    """Helper method for a json response

        Args:
            data An dictionary object
            key In which will be included data
    """
    return {key: data}
