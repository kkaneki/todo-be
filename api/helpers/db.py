"""Database helpers"""


def is_int(value):
    """Checks whether an argument
    can be converted into integer type

        Args:
            value A argument which need to check

        Returns:
            Boolean type True/False
    """
    try:
        int(value)
        return True
    except ValueError:
        return False
