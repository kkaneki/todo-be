"""ToDo schema"""

from marshmallow import Schema, fields, validate


class TodoSchema(Schema):
    """
        Attributes:
            name A name of a ticket
           __fields__ An allowed attributes for an update
    """
    name = fields.Str(required=True, validate=validate.Length(min=3))

    __fields__ = ['name']
