"""Base model"""

from peewee import *  # noqa

db = SqliteDatabase('test.db')  # noqa


class BaseModel(Model):  # noqa
    """Base class for all models"""
    class Meta:
        database = db
