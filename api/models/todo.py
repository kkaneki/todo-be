"""Todo model"""

import datetime

from api.models import BaseModel, BooleanField, CharField, DateTimeField


class Todo(BaseModel):
    """
        Attributes:
            name(string, required): An name of a ticket
            completed(boolean): Marks ticket as completed
            default is `False`
            created_at(datetime): Date and time when ticket was created
            updated_at(datetime: Date and time when ticket was updated

        Methods:
            info Returns a dictionary, with ticket information
    """
    name = CharField(unique=True, index=True)
    completed = BooleanField(default=False)
    created_at = DateTimeField(default=datetime.datetime.utcnow())
    updated_at = DateTimeField(null=True)

    def info(self):
        return {
            'id': self.id,
            'createdAt': self.created_at.isoformat(),
            'name': self.name,
            'completed': False,
        }

    @classmethod
    def ticket_exists(cls, ticket_id):
        """Checks whether ticket is present in our table

            Args:
                ticket_id Which need to check

            Returns:
                Boolean type, True/False
        """
        return cls.select().where(Todo.id == ticket_id).exists()
