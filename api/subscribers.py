"""Subscriber helpers"""


def add_cors_headers_response_callback(event):
    """Helper subscriber for CORS"""
    def cors_headers(request, response):
        if request.method == 'OPTIONS':
            response.status_code = 200
        response.headers.update({
        'Access-Control-Allow-Origin': 'http://localhost:3000',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Max-Age': '1728000',
        })
    event.request.add_response_callback(cors_headers)
