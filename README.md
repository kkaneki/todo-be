# Todo-BE

* **Technologies**
    * Python 3.6
    * Pyramid
    * Peewee

## Install

First you need to install all packages
```pip install -r requirements-dev.txt```

Then you need to initialize database
```python initialize.py```

And after that, you able to run server
```python run.py```
