from wsgiref.simple_server import make_server

from peewee import IntegrityError
from pyramid.config import Configurator
from pyramid.events import NewRequest

from api.handlers import integreti_error_view
from api.subscribers import add_cors_headers_response_callback


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.add_subscriber(add_cors_headers_response_callback, NewRequest)
    config.add_route('todos', '/api/todo')
    config.add_route('create_ticket', '/api/todo/new')
    config.add_route('update_ticket', '/api/todo/{id}/toggle')
    config.scan('api.views')
    config.add_view(integreti_error_view,
                    context=IntegrityError,
                    renderer='json')
    return config.make_wsgi_app()


if __name__ == '__main__':
    app = main({})
    server = make_server('0.0.0.0', 6543, app)
    server.serve_forever()
